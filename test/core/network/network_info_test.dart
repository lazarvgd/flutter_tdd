import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/core/network/network_info.dart';
import 'package:mockito/mockito.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

main() {
  MockDataConnectionChecker dataConnectionChecker;
  NetworkInfo networkInfo;

  setUp(() {
    dataConnectionChecker = MockDataConnectionChecker();
    networkInfo = NetworkInfoImpl(dataConnectionChecker);
  });
  group('isConnected', () {
    test('should ', () async {
      // arrange
      final tHasConnectionFuture = Future.value(true);
      when(dataConnectionChecker.hasConnection).thenAnswer((_) => tHasConnectionFuture);

      // act
      var result = networkInfo.isConnected;

      // assert
      verify(dataConnectionChecker.hasConnection);
      expect(result, tHasConnectionFuture);
    });
  });
}
