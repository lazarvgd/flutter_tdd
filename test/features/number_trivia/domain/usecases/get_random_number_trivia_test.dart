import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/core/usecases/usecase.dart';
import 'package:fluttertdd/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:fluttertdd/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:fluttertdd/features/number_trivia/domain/usecases/get_random_number_trivia.dart';
import 'package:mockito/mockito.dart';

class MockNumberTriviaRepository extends Mock implements NumberTriviaRepository {}

main () {
  GetRandomeNumberTrivia usecase;
  MockNumberTriviaRepository repository;

  setUp(() {
    repository = MockNumberTriviaRepository();
    usecase = GetRandomeNumberTrivia(repository);
  });

  final tNUmberTrivia = NumberTrivia(number: 1, text: 'test');

  test('should get trivia from the repo', () async {
    // arrange
    when(repository.getRandomNumberTrivia()).thenAnswer((realInvocation) async => Right(tNUmberTrivia));

    // act
    final result = await usecase(NoParams());

    // assert
    expect(result, Right(tNUmberTrivia));
    verify(repository.getRandomNumberTrivia());
    verifyNoMoreInteractions(repository);
  });
}