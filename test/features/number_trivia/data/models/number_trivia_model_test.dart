import 'dart:convert';
import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:fluttertdd/features/number_trivia/domain/entities/number_trivia.dart';

import '../../../../core/fixtures/fixture_deader.dart';

main() {
  final tNumberTriviaModel = NumberTriviaModel(number: 1, text: 'test');
  test('should be a subclass of NumberTrivia entity', () {
    expect(tNumberTriviaModel, isA<NumberTrivia>());
  });

  group('fromJson', () {
    test('should return a valid model when the json number is integer', () {
      // arragne
      final Map<String, dynamic> jsonMap = json.decode(fixture('trivia.json'));

      // act
      final result = NumberTriviaModel.fromJson(jsonMap);

      // assert
      expect(result, equals(tNumberTriviaModel));
    });

    test('should return a valid model when the json number is double', () {
      // arragne
      final Map<String, dynamic> jsonMap = json.decode(fixture('trivia_double.json'));

      // act
      final result = NumberTriviaModel.fromJson(jsonMap);

      // assert
      expect(result, equals(tNumberTriviaModel));
    });
  });


  group('toJson', () {
    test('should return a valid json map containing proper data', () {
      // arragne

      // act
      final result = tNumberTriviaModel.toJson();
      
      // assert
      expect(result, {'text':'test', 'number':1});
    });
  });
}