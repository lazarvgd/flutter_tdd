import 'package:dartz/dartz.dart';
import 'package:fluttertdd/core/error/exceptions.dart';
import 'package:fluttertdd/core/error/failures.dart';
import 'package:fluttertdd/core/network/network_info.dart';
import 'package:fluttertdd/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:fluttertdd/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:fluttertdd/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:fluttertdd/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:meta/meta.dart';

class NumberTriviaRepositoryImpl implements NumberTriviaRepository {
  NumberTriviaRemoteDataSource remoteDataSource;
  NumberTriviaLocalDataSource localDataSource;
  NetworkInfo networkInfo;

  NumberTriviaRepositoryImpl({@required this.remoteDataSource, @required this.localDataSource, @required this.networkInfo});

  @override
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(int number) async {
    return await _getTrivia(() async => await remoteDataSource.getConcreteNumberTrivia(number));
  }

  @override
  Future<Either<Failure, NumberTrivia>> getRandomNumberTrivia() async {
    return await _getTrivia(() async => await remoteDataSource.getRandomNumberTrivia());
  }

  Future<Either<Failure, NumberTrivia>> _getTrivia(Future<NumberTrivia> Function() function) async {
    if (await networkInfo.isConnected) {
      try {
        final resultTrivia = await function();
        localDataSource.cacheNumberTrivia(resultTrivia);
        return Right(resultTrivia);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTrivia = await localDataSource.getLastNumberTrivia();
        return Right(localTrivia);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
