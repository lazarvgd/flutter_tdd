import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/core/error/exceptions.dart';
import 'package:fluttertdd/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:fluttertdd/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/fixtures/fixture_deader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

main() {
  NumberTriviaLocalDataSourceImpl dataSource;
  SharedPreferences sharedPreferences;

  setUp(() {
    sharedPreferences = MockSharedPreferences();
    dataSource = NumberTriviaLocalDataSourceImpl(sharedPreferences: sharedPreferences);
  });
  group('getLAst number trivia', () {
    final tNumberTriviaModel = NumberTriviaModel.fromJson(json.decode(fixture('trivia_cached.json')));

    test('should return number model from shared pref. when there is one int the cache', () async {
      // arrange
      when(sharedPreferences.getString(any)).thenReturn(fixture('trivia_cached.json'));

      // act
      final result = await dataSource.getLastNumberTrivia();
      // assert
      verify(sharedPreferences.getString('CACHED_NUMBER_TRIVIA'));
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw CacheException where there is no a cached value', () async {
      // arrange
      when(sharedPreferences.getString(any)).thenReturn(null);

      // act
      final call = dataSource.getLastNumberTrivia;
      // assert
      expect(() => call(), throwsA(isA<CacheException>()));
    });
  });
  group('cache number trivia', () {
    final tNumberTriviaModel = NumberTriviaModel(text: 'test', number: 1);
    test('should call shared prefrences to cache the data', () {
      // act
      dataSource.cacheNumberTrivia(tNumberTriviaModel);
      // assert
      final expectedJsonString = json.encode(tNumberTriviaModel.toJson());
      verify(sharedPreferences.setString('CACHED_NUMBER_TRIVIA', expectedJsonString));
    });
  });
}
