import 'package:dartz/dartz.dart';
import 'package:fluttertdd/core/error/failures.dart';
import 'package:fluttertdd/features/number_trivia/domain/entities/number_trivia.dart';

abstract class NumberTriviaRepository {
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(int number);
  Future<Either<Failure, NumberTrivia>> getRandomNumberTrivia();
}
