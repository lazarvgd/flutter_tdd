import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:fluttertdd/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:fluttertdd/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:mockito/mockito.dart';

class MockNumberTriviaRepository extends Mock implements NumberTriviaRepository {}

main() {
  GetConcreteNumberTrivia usecase;
  MockNumberTriviaRepository repository;

  setUp(() {
    repository = MockNumberTriviaRepository();
    usecase = GetConcreteNumberTrivia(repository);
  });

  final tNumber = 1;
  final tNUmberTrivia = NumberTrivia(number: 1, text: 'test');

  test('should get trivia for the number from the repo', () async {
    // arrange
    when(repository.getConcreteNumberTrivia(any)).thenAnswer((realInvocation) async => Right(tNUmberTrivia));

    // act
    var params = Params(number: tNumber);
    final result = await usecase(params);

    // assert
    expect(result, Right(tNUmberTrivia));
    verify(repository.getConcreteNumberTrivia(tNumber));
    verifyNoMoreInteractions(repository);
  });
}
