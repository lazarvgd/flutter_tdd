import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:fluttertdd/core/error/exceptions.dart';
import 'package:fluttertdd/features/number_trivia/data/datasources/number_trivia_remote_data_source.dart';
import 'package:fluttertdd/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../../../core/fixtures/fixture_deader.dart';

class MockHttp extends Mock implements http.Client {}

main() {
  NumberTriviaRemoteDataSource dataSource;
  MockHttp httpClient;

  setUp(() {
    httpClient = MockHttp();
    dataSource = NumberTriviaRemoteDataSourceImp(client: httpClient);
  });

  void setupMockHttpClient200(){
    when(httpClient.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response(fixture('trivia.json'), 200));
  }

  void setupMockHttpClientFailure404(){
    when(httpClient.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response('wrong', 404));
  }

  group('get concrete number trivia', () {
    final tNumber = 1;
    final tNumberTriviaModel = NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));
    test('should perform a GET request on a url with number being the endpoint and with application/json header', () {
      // arrange
      setupMockHttpClient200();
      // act
      dataSource.getConcreteNumberTrivia(tNumber);

      // assert
      verify(httpClient.get('http://numbersapi.com/$tNumber', headers: {'Content-Type': 'application/json'}));
    });

    test('should return number trivia when response code is 200', () async {
      // arrange
      setupMockHttpClient200();
      // act

      final result = await dataSource.getConcreteNumberTrivia(tNumber);
      // assert
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw a server exception when the response code is 404 or other', () {
      // arrange
      setupMockHttpClientFailure404();
      // act
      final call = dataSource.getConcreteNumberTrivia;

      // assert
      expect(()=>call(tNumber), throwsA(isA<ServerException>()));
    });
  });

  //======

  group('get random number trivia', () {
    final tNumberTriviaModel = NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));
    test('should perform a GET request on a url with random endpoint and with application/json header', () {
      // arrange
      setupMockHttpClient200();

      // act
      dataSource.getRandomNumberTrivia();

      // assert
      verify(httpClient.get('http://numbersapi.com/random', headers: {'Content-Type': 'application/json'}));
    });

    test('should return number trivia when response code is 200', () async {
      // arrange
      setupMockHttpClient200();
      // act

      final result = await dataSource.getRandomNumberTrivia();
      // assert
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw a server exception when the response code is 404 or other', () {
      // arrange
      setupMockHttpClientFailure404();
      // act
      final call = dataSource.getRandomNumberTrivia;

      // assert
      expect(()=>call(), throwsA(isA<ServerException>()));
    });
  });
}
